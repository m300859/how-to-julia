# How to start with Julia

This tutorial will give you the basic knowledge to use Julia in your daily work at the institute. It will start with an installation guide followed by the first example code. In later chapters, we will discuss more advanced themes like how to load and store climate data, increase the speed, and run a task in parallel.

All the upcoming chapters are structured in the same way. Each chapter is dedicated to one aspect of the language. They begin with some simple and often used functionality. The more advanced sections are handled later. It is recommended to read the first section of every chapter to get an overview of the features. But it is not important to read all the sections from a chapter at once.

One exception is the [interactive command line](#julia-interactive-command-line) chapter. This chapter will give you a very basic overview of all the important topics that are discussed in more detail later on.

## What is Julia
Before we start with the installation and the first examples I want to take a first look at Julia. Julia is a very young and modern programming language. It was announced in 2012 and version 1.0 was published in August 2018. In comparison, FORTRAN is relatively old language. The first widely used standard was published in 1966 by the American National Standard Institute (ANSI).

Julia is what we called, a dynamically typed, functional programming language. This means that you can write code without declaring any types of variables or functions like in C or FORTRAN. Many modern languages like Python or JavaScript use this technique. As well as Julia orientated at the background of the techniques, Julia is also inspired by the syntax.  If you are familiar with Python's syntax it will be very easy for you to switch to Julia.

But Julia has one big advantage over the other modern languages named here; *speed*. While Python and JavaScript are interpreted languages, Julia has a workflow under the hood to compile to machine code at runtime. This machine code is identical to the one we know from FORTRAN or C. In small [laboratory benchmarks](https://julialang.org/benchmarks/) Julia can achieve the speed of FORTRAN.

# Install Julia
In this chapter, I will show you how to install Julia in different environments.

## Ubuntu / Debian
Julia is embedded in the Debian package manager. This makes it very simple to install Julia if you have root rights.
``` bash
sudo apt update
sudo apt install julia
```
Julia is a very young programming language with a high update frequency. It might be a good idea to check if there are updates available.
``` bash
sudo apt update
sudo apt upgrade
```

## MPI-M
It is quite easy to use Julia on the MPI-M computers. Just load the module and start your first test.
```bash
module load julia/1.5.3
```

If you want to use Julia in the future on MPI-M systems, it could be a good idea to add this line to your `~/.bashrc` file. This file is loaded automatically after log on to your machine. After updating your `~/.bashrc` you must reopen the terminal or run this update command: `source ~/.bashrc`.

## mistral
Julia is installed as a module on mistral. First, you have to set the module source, before you can load the Julia module.
```bash
module use /sw/spack-rhel6/spack/modules/linux-rhel6-haswell
module load julia/1.5.2-gcc-9.1.0
```

If you want to use Julia in the future on mistral, it could be a good idea to add these two lines to your `~/.bashrc` file. This file is loaded automatically after login on the mistral login node. After updating your `~/.bashrc` you must log in again or run this update command: `source ~/.bashrc`.

## Windows
Julia is also available on Microsoft Windows. You can download an installer [here](https://julialang.org/downloads/). I will not recommend using the Windows version. Although it is fully supported by Julia, examples later in this tutorial will only be responsive to Unix-based systems.

If you love your Windows like I do but also need a Unix-based system sometimes you definitely have to take a look at the [Windows subsystem for Linux (WSL)](https://www.microsoft.com/en-us/p/ubuntu/9nblggh4msv6?activetab=pivot:overviewtab).

## macOS
To install Julia on macOS please download the installation file from the [download portal](https://www.julialang.org/downloads/). However, some of the examples within the following chapters should not work out of the box. To use all of the here shown features without problems please use a Linux environment.

# Julia interactive command line
After you have installed Julia correctly we want to start with our first examples. For these you have to start the interactive Julia command line.
``` julia
m300859$ julia
               _
   _       _ _(_)_     |  Documentation: https://docs.julialang.org
  (_)     | (_) (_)    |
   _ _   _| |_  __ _   |  Type "?" for help, "]?" for Pkg help.
  | | | | | | |/ _` |  |
  | | |_| | | | (_| |  |  Version 1.4.1
 _/ |\__'_|_|_|\__'_|  |  Ubuntu ⛬  julia/1.4.1+dfsg-1
|__/                   |

julia>   
```
This command line will be our playground for the first steps. It works similar to the command line that you maybe know from Python. You can write any command to this terminal and it will evaluate immediately. After the evaluation, the result is printed to the command line. Now you can write your next command. This is also the reason why this command line is often called **REPL**. REPL stands for **r**ead **e**valuate **p**rint **l**oop.

If you want to add two numbers you can easily write it directly to the REPL.
``` julia
julia> 15 + 20
35

julia> 19 / 2
9.5

julia> 1.2 * 0.8e12
9.6e11
```

It is not very handy to remember all results in a complex calculation and type them into the formula at the correct position. For this Julia provides a special variable. `ans` hold the result from your last command.
``` julia
julia> 1.2 * 0.8e12
9.6e11

julia> ans
9.6e11
```

## Variables
With `ans` we opened the next important chapter: variables. Variables are one of two main pillars of imperative programming languages. As I mentioned before variables are dynamically typed. This means that you do not have to declare the variable before you can use it. For comparison, if you want to use a variable in FORTRAN you will declare and assign this variable like this:
```fortran
real :: speed
speed = 2.4
```

In Julia, you can write:
``` julia
julia> speed = 2.4
```

If you want to know the value of a variable you can type the name directly into the REPL. 
``` julia
julia> speed = 2.4
2.4

julia> speed
2.4
```

The fact that variables in Julia do not have to be typed explicitly does not mean that no types exist at all. We want to have a closer look at the Julia type system later in this tutorial. For now, it will be enough to know the basic types. If you write a literal constant like `1` or `2.0e11` this literal is related to a specific type.
- Integer Values like `1` or `-2` are represented by `Int64`.
- Floating Point values like `0.0`, `1.2` or `8.0e12` are represented by `Float64`.
- Strings like `"Hello World"` would be stored in a `String` variable.

If you don't know the current type of variable, you can ask Julia by using the `typeof()` function.
``` julia
julia> typeof(12)
Int64

julia> typeof(1.2)
Float64

julia> typeof(1+2.3)
Float64

julia> typeof("Hello World")
String
2.4
```

What is very special for dynamically typed languages like Python and Julia, is that a variable can change its type. 
``` julia
julia> var = 3
3

julia> typeof(var)
Int64

julia> var = 9.9
9.9

julia> typeof(var)
Float64
```

For now, it is enough that you have seen this effect, but we will discuss this topic later in the [types chapter](#types) again.

## Basic Functions
While variables are the first pillar of imperative programming languages, functions are the second. A function takes **zero or more** parameters as an input and generates **one or more** results as an output. Especially it needs some time to get used to how the output is handled in Julia functions. Any function should return at least one value. You can not write a `subroutine` like in FORTRAN or a `void` function like in C.

If you want to declare a function in Julia, you have two possible ways. The first is oriented to mathematical functions. This version has the same power as the second but it should only be used for simple expressions. The second follows more the typical programming language syntax.
``` julia
julia> add(a, b, c) = a + b + c
add (generic function with 1 method)

julia> function sub(a, b, c)
         a - b - c
       end
sub (generic function with 1 method)

julia> function addMul(a, b, c)
         return (a + b) * c
       end
addMul (generic function with 1 method)
```

As you see, it is possible to use a return statement, but not necessary. After you have declared a function, you can use it like the `typeof()` function before. 
``` julia
julia> add(2, 2.2, 3)
7.2

julia> sub(80, 12, 0)
68
```

If you want to dive deeper into the Julia function system, please read the [functions chapter](#Functions).

## Basic Arrays
Programming without arrays is only half as much fun. This is the reason why we want to learn how to use them next. If you want to use an array, you have two different ways. The first is to declare and define the values together with a literal expression.
``` julia
julia> arr1 = [1,2,3]
3-element Array{Int64,1}:
 1
 2
 3
```

The second is to declare the array to reserve enought memory and initialize the values later.
``` julia
julia> arr2 = Array{Int64, 1}(undef, 3)
3-element Array{Int64,1}:
 140372968079008
 140372807975280
 140372834728928

julia> arr2[1] = 4; arr2[2] = 5; arr2[3] = 6
5

julia> arr2
3-element Array{Int64,1}:
 4
 5
 6
```
We see in both cases that Julia creates an array with three integer elements in one dimension: `3-element Array{Int64,1}`. But in the second example in which we allocate the memory without initialization, the values that are stored at the beginning are random. You have to set the elements to correct values first, before we could use the array how we expected. The first element in the array has always the index `1`.

If you do not know how many elements you have to store in the array, you can create an array with the size of zero and append to it step by step.
``` julia
julia> arr3 = Array{Int64, 1}(undef, 0)
0-element Array{Int64,1}

julia> append!(arr3, 2)
1-element Array{Int64,1}:
 2

julia> append!(arr3, 5)
2-element Array{Int64,1}:
 2
 5

julia> arr3
2-element Array{Int64,1}:
 2
 5
```

If you want to know what the `!` means, please read the [notation section](#notation). For further information about arrays and other container types, please read the [container types](#container-types) chapter.

## Flow control
No program would work without flow control. In the following section, you will learn how to make decisions in Julia and run expressions multiple times.

### If, Ifelse and else
Like in most other language too, Julia supports if else structures. Any if else structure must end with an `end`.
``` julia
julia> if a == c
           println("a == c")
       elseif c < 5
           println("c < 5")
       else
           println("strange value")
       end
```

If you want to combine two comparisons you can use the boolean operators `&&` as an **and** while the `||` operator works as an **or**. These operators are lazily evaluated. This mean that this expression `a < 5 && b > 8` will first evaluate the `a < 5` and only if this returns true `b > 8` would be evaluated. The opposite happens with the `||` operator. 

This 	technique is sometimes useful for writing very short if structures. For example, a debug message is output only if the value of `a` is equal to five, when using the following syntax:
``` julia
julia> a = 5
5

julia> a == 5 && println("abc")
abc
```

### Loops
Julia has two types of loops. The more generic `while` loop that loops while the particular expression is true and the `for` loop that loops over a set of values.
``` julia
julia> i = 0
0

julia> while i < 5
         println("new line ", i)
         global i = i + 1
       end
new line 0
new line 1
new line 3
new line 3
new line 4
```

The `global` in front of `i` is only allowed if you run this while loop outside a function in a global context like the REPL. The keyword gives the compiler the hint that `i` was already defined outside the loop. 

The `for` loop defines a variable, in our case `j`, which is only known inside the loop. `j` iterates over the values of the array.
``` julia
julia> arr = [1, 2, 3, 4, 5]
5-element Array{Int64,1}:
 1
 2
 3
 4
 5

julia> for j in arr
         println("Number ", j)
       end
Number 1
Number 2
Number 3
Number 4
Number 5
```

If you do not want to create an array that contains numbers from 1 to n for every simple for loop, you can use the `OneTo` function. For this, you have to import the `Base.OneTo` function. Now you could create a more efficient and readable `for` loop.
``` julia
julia> import Base.OneTo

julia> for i in OneTo(5)
         println(i)
       end
1
2
3
4
5
```

## Package manager
Not all functionalities can be delivered by the language itself. For some problems, Julia has a standard library, for other problems a big open-source repository with thousands of modules exists. 

If you want to install a module, that is not included in the standard library, you can use the built-in package manager. For this, you have to press the `]` key on your keyboard while you are working inside the Julia REPL. Now you can install the respective package with the `add` command. If the package that you're trying to install has dependencies on other packages, these dependencies will be automatically installed as well.
``` julia
julia>]

(@v1.4) pkg> add DataFrames
  Resolving package versions...
  Installed Crayons ─────────── v4.0.4
  Installed Compat ──────────── v3.25.0
  Installed StructTypes ─────── v1.2.1
  Installed CategoricalArrays ─ v0.9.0
  Installed DataFrames ──────── v0.22.2
  Installed PrettyTables ────── v0.10.1
   Updating `~/.julia/environments/v1.4/Project.toml`
  [a93c6f00] + DataFrames v0.22.2
   Updating `~/.julia/environments/v1.4/Manifest.toml`
  [324d7699] + CategoricalArrays v0.9.0
  [34da2185] + Compat v3.25.0
  [a8cc5b0e] + Crayons v4.0.4
  [a93c6f00] + DataFrames v0.22.2
  [41ab1584] + InvertedIndices v1.0.0
  [2dfb63ee] + PooledArrays v0.5.3
  [08abe8d2] + PrettyTables v0.10.1
  [856f2bd8] + StructTypes v1.2.1
  [9fa8497b] + Future
```

If you want to remove a package again, you can use the `remove` command.
``` julia
    (@v1.4) pkg> remove DataFrames
   Updating `~/.julia/environments/v1.4/Project.toml`
  [a93c6f00] - DataFrames v0.22.2
   Updating `~/.julia/environments/v1.4/Manifest.toml`
  [324d7699] - CategoricalArrays v0.9.0
  [34da2185] - Compat v3.25.0
  [a8cc5b0e] - Crayons v4.0.4
  [a93c6f00] - DataFrames v0.22.2
  [41ab1584] - InvertedIndices v1.0.0
  [2dfb63ee] - PooledArrays v0.5.3
  [08abe8d2] - PrettyTables v0.10.1
  [856f2bd8] - StructTypes v1.2.1
  [9fa8497b] - Future
```

Julia and its modules are written by a huge community. All the parts are updated frequently. This is the reason why it is recommended to update your packages often. For this, you can easily type `update` in the package manager.
``` julia
(@v1.4) pkg> update
   Updating registry at `~/.julia/registries/General`
   Updating git-repo `https://github.com/JuliaRegistries/General.git`
   Updating git-repo `https://gitlab.dkrz.de/m300859/cdi.jl.git`
  Installed Hwloc_jll ────── v2.2.0+0
  Installed LibSSH2_jll ──── v1.9.0+3
...
  Installed Requires ─────── v1.1.2
Downloading artifact: LibSSH2
Downloading artifact: Hwloc
Downloading artifact: HDF5
Downloading artifact: LibCURL
Downloading artifact: nghttp2
Downloading artifact: NetCDF
   Updating `~/.julia/environments/v1.4/Project.toml`
   Updating `~/.julia/environments/v1.4/Manifest.toml`
  [ec485272] ↓ ArnoldiMethod v0.0.4 ⇒ v0.0.2
  [9e28174c] - BinDeps v1.0.2
  ...
  [8e850ede] + nghttp2_jll v1.40.0+2
   Building GR ───────→ `~/.julia/packages/GR/cRdXQ/deps/build.log`
   Building TimeZones → `~/.julia/packages/TimeZones/K98G0/deps/build.log`
   Building cdi ──────→ `~/.julia/packages/cdi/PokMy/deps/build.log`
```

To exit the package manager again use the `back space` key.

## Use modules
If you want to use a function that is delivered by a package that you have installed, you can import this module with the `using` keyword. If the package is not installed yet please read the [package manager section](#package-manager).
``` julia
julia> now()
ERROR: UndefVarError: now not defined
Stacktrace:
 [1] top-level scope at REPL[48]:1

julia> using Dates

julia> now()
2020-12-18T15:11:07.13
```

## Tips & tricks
The Julia interactive command line (REPL) is the Swiss army knife for a Julia developer. In this section, I documented some useful features that are supported by the REPL.

- The auto-completion: If you type the beginning of command into the REPL, you can use the auto-completion by pressing the **Tab** key. This makes development much faster.
- The built-in user manual: To open the built-in user manual press the `?` at beginning of a line. Now type in any keyword, a function, a variable, or a module name for which you want to read the documentation. Most modules support this technique.
- Recall a command: You can move with the arrow keys in your own Julia history. With this feature, it is much simpler to retry a complex function call.

# Set up a Julia project
The Julia interactive command line (REPL) is a powerful tool. But its state is not persistent. If you want to execute a program multiple times, you have to write the commands again. And more important, if you close the command line and reopen it you have to define all variables and functions again. To write more persistent code, you can write files of code that can be interpreted by Julia. In the next steps we want to set up a project together.

1. Create a new folder structure like this:
```bash
MyProject/
└── src
```

2. Now enter the new project directory. Afterward, open the Julia interactive command line with the `--project=@.` flag. This flag tells Julia that the current directory is now our working directory. If the command line is opened, type `]` to enter the package manager:
```bash
cd MyProject
julia --project=@.
               _
   _       _ _(_)_     |  Documentation: https://docs.julialang.org
  (_)     | (_) (_)    |
   _ _   _| |_  __ _   |  Type "?" for help, "]?" for Pkg help.
  | | | | | | |/ _` |  |
  | | |_| | | | (_| |  |  Version 1.4.1
 _/ |\__'_|_|_|\__'_|  |  Ubuntu ⛬  julia/1.4.1+dfsg-1
|__/                   |

(MyProject) pkg>  
```

3. Now you can activate your new project:
``` julia
(MyProject) pkg> activate
 Activating environment at `~/MyProject/Project.toml`
```

4. Most projects need external modules. These external modules can be a part of the standard library or originate from an external source. Before you can use these modules with the `using` command inside your code, you must add these modules to the dependency graph. How to add or remove dependency has been discussed in the [Package manager](#package-manager) section. For our example, we use the `Dates` module from the standard library.
``` julia
(MyProject) pkg> add Dates
   Updating registry at `~/.julia/registries/General`
   Updating git-repo `https://github.com/JuliaRegistries/General.git`
  Resolving package versions...
   Updating `~/MyProject/Project.toml`
  [ade2ca70] + Dates
   Updating `~/MyProject/Manifest.toml`
  [ade2ca70] + Dates
  [de0858da] + Printf
  [4ec0a83e] + Unicode
```

5. If you call the status command in the package manager you can see that the package was successfully added:
``` julia
Status `~/MyProject/Project.toml`
  [ade2ca70] Dates
```

6. To exit Julia press the `backspace` key and type `exit()`. Now you can see that Julia creates new files in our project directory.
```bash
MyProject/
├── Manifest.toml
├── Project.toml
└── src
```

7. Now it is time to write our first program. Add a new file `myProject.jl` in the `/src` folder and write the following lines:
```julia
using Dates

println("Now it is ", now())
```

8. Your project folder should have the following structure:
```bash
MyProject/
├── Manifest.toml
├── Project.toml
└── src
    └── myProject.jl
```

9. In the last step execute your first program written in Julia. For this purpose, we call Julia again with the `--project=@.` flag followed by the source file that we want to execute.
```bash
julia --project=@. src/myProject.jl
Now it is 2020-12-26T12:17:59.437
```

10. If you want to add more modules please go back to steps two to five.

## Additional information
If you want to know more about the functions of the package manager you can read the well-written [documentation](https://julialang.github.io/Pkg.jl/v1.5/).

# Execute a Julia Project
If you get a new Julia project it is quite easy to execute it the first time. Please enter the project directory and instantiate the project. This is only important the first time you run the code since the dependencies are then installed automatically.
``` bash
julia --project=@. -e "import Pkg; Pkg.instantiate()"
```

Now you can execute your project as usual.

## Mistral
To use a project code on Mistral you have to be on the login-node. Here you can instantiate your project as shown before. To add the project to the job queue you can create a `SBATCH` file or use the following command:
``` bash
srun --partition=XXXXX --nodes=1 --account=XXXX --time=01:00:00 --output=tem_calc.o%j --error=tem_calc.e%j --job-name=tem_calc julia --project=@. tem-calc.jl

``` 

# Functions
In this chapter, we want to dive deeper into functions in Julia. The first fundament was set in the [basic functions](#basic-functions) section before.

## Multiple return values
As I mentioned before, in Julia it is possible to return more than one value at a time. This can be very handy sometimes. The following function takes two parameters and returns them in the opposite order.
``` julia
julia> function switchParameter(a, b)
         return b, a
       end
switchParameter (generic function with 1 method)

julia> c, d = switchParameter(1,2)
(2, 1)

julia> c
2

julia> d
1
```
The same function can be declared as a one line function `switchParameter(a, b) = b, a`.

## Notation
The Julia syntax is orientated on popular programming languages like Python or C. All these syntaxes try to be as intuitive and easy to read as possible. One big advantage of the Julia syntax is the `!` notation. If a function name ends with a `!`, this function has an effect on at least one input parameter. The official documentation says that the first function argument will change, but some developers outside the original Julia standard library interpret the syntax a bit differently.

The following example will append the number `4` to the array `arr` that contains the values one to three. As any function, `append!()` will return a value, in this case, the changed array. But at the same time `append!()` has changed the original array `arr` as well.
``` julia
julia> arr = [1,2,3]
3-element Array{Int64,1}:
 1
 2
 3

julia> append!(arr, 4)
4-element Array{Int64,1}:
 1
 2
 3
 4

julia> arr
4-element Array{Int64,1}:
 1
 2
 3
 4
```

## Return type
If you compare the Julia function declaration and a call of a FORTRAN or C function you see again that there are no type declarations at all. The input and output types will be analyzed at runtime. This makes it easy to write functions for different types. We can print the calculated return type of our `add()` function with `typeof()`.
``` julia
julia> typeof(add(1, 2, 3))
Int64

julia> typeof(add(1, 2.2, 3))
Float64
```

What we can see is that if we add one floating-point value to the calculation the total return type will change automatically. This can be a curse and a blessing at the same time. We will look at why this is so later in this tutorial.

## Typed Parameters
While we used the automatic type system from Julia that select the correct type for our case at runtime most of the time, this is not the best solution sometimes. Some algorithms are not available for a special type or it could be useful to define specialization for different types that work better in a more defined environment.

A good case for this is the `append!()` function that adds a new value to an existing array. While the default implementation with one dimensional `AbstractArray{T, 1}` works most of the time, there is a better implementation for a `BitArray`. BitArrays can store only `true` and `false` values and can be handled more efficiently by the CPU if they are not stored in the default way.
``` julia
julia> methods(append!)
# 5 methods for generic function "append!":
[1] append!(A::Array{Bool,1}, items::BitArray{1}) in Base at bitarray.jl:767
[2] append!(B::BitArray{1}, items::BitArray{1}) in Base at bitarray.jl:751
[3] append!(a::Array{T,1} where T, items::AbstractArray{T,1} where T) in Base at array.jl:953
[4] append!(B::BitArray{1}, items) in Base at bitarray.jl:766
[5] append!(a::AbstractArray{T,1} where T, iter) in Base at array.jl:960
```

During runtime, Julia selects the best option for our case. This method is called **multiple dispatch**. You will read this in all kinds of Julia documentations. The multiple dispatch is one of the main reasons why Julia can be as fast as it is. In the default implementation, there are more than 350 special implantations for the `*` operator.

If you want to write a specialization on your own, you can do this by adding a type information to the function parameter.
``` julia
julia> function f(a::Int64)
         println("called with an Interger ", a)
       end

julia> function f(a::Float64)
         println("called with a Float ", a)
       end

julia> function f(a::Bool)
         println("called with a Bool ", a)
       end
f (generic function with 3 methods)

julia> f(6)
called with an Interger 6

julia> f(6.654)
called with a Float 6.654

julia> f(true)
called with a Bool true
```

If you want to know more about which types you can use in a function and how the Julia type system works, please read the [Types chapter](#types).

## Default values
Julia allows the developer to define default values for function parameters. To use this feature you must declare the parameters which should have a default value as the last parameters in the function header. We will define our own `add(a, b, c=0)` function which can add two or three values.
``` julia
julia> function add(a, b, c=0)
         a + b + c
       end
add (generic function with 2 methods)
```

The built-in `methods()` function shows you all possible function versions that you could call. In our example, you can see that Julia creates two methods from one function. This is the same as if we created both methods by ourselves. The automatically created methods can save us typing and make the code more readable.
``` julia
julia> methods(add)
# 2 methods for generic function "add":
[1] add(a, b) in Main at REPL[1]:2
[2] add(a, b, c) in Main at REPL[1]:2

julia> function add(a, b)
         a + b + 0
       end

julia> function add(a, b, c)
         a + b + c
       end
```

Now you can use our add function as usual.
``` julia
julia> add(1, 2)
3

julia> add(1, 2, 3)
6
```

## Named parameters
Another useful Julia feature, is the named function parameter, especially in combination with [default values](#default-values). If you have a function that could have a lot of different parameters, but most parameters are set to their default values and only a few are defined in the calling sequence, you can create your function as follows:
``` julia
julia> function mySort(arr; rev=false)
         if rev
            return reverse(sort(arr))
         else
            return sort(arr)
         end
      end
mySort (generic function with 1 method)
```

The input array in our sort example is mandatory. But the *revers* flag will be set only in a few cases. Named parameters must always be declared at the end of the function header following a semicolon. If you have multiple named parameters, they will have to be separated again by a comma.

You can call our example above with and without the *revers* flag.
``` julia
julia> arr = [1,3,2]

julia> mySort(arr)
3-element Array{Int64,1}:
 1
 2
 3

julia> mySort(arr, rev=true)
3-element Array{Int64,1}:
 3
 2
 1
```

The full flexibility becomes apparent when you have multiple named parameters. For example, in the standard Julia sort implementation `sort(A::AbstractArray; dims, alg, lt, by, rev, order)` all parameters after the semicolon are called named parameters. If you want to call this function and use some of these parameters the order does not matter. We will use the *reverse* flag before the *algorithm* flag in contrast to the function declaration before.
``` julia
julia> sort(arr, rev=true, alg=MergeSort)
3-element Array{Int64,1}:
 3
 2
 1
```

# Container types
Containers can hold more than one value at a time. Arrays are the most popular containers in scientific computation. Tuples work like arrays, but they have a constant size and can't be changed after the construction. Dictionaries hold key-value pairs.

## Array
We mentioned the basics of arrays in the [Basic Arrays](#basic-arrays) section before. Now we can dive deeper into the array philosophy in Julia. Arrays have the reputation that they are fast. This is possible because the values, of one specific type, are stored in a contiguous memory strip. In these examples, most arrays store `Int64` values. 

### Array declaration
It is possible to create arrays in two different ways. The first is to write all values as literal. An array literal is a comma-separated list bordered by edge brackets `arr = [1,2,3]`. In this case, Julia selects the correct type. The second way is to declare an array with a type and a size. In this case Julia reserves the required memory, but the elements of this array are filled with random values.
``` julia
julia> arr = Array{Int64, 1}(undef, 3)
3-element Array{Int64,1}:
 140372968079008
 140372807975280
 140372834728928
 ```

We want to analyze the declaration line `Array{Int64, 1}(undef, 3)` in detail. First, we can split up the line into two parts: the type declaration `Array{Int64, 1}` and the following array definition. The first half creates a type like `Ìnt64`, `Float64` or `String`. In this case the type `Array{Int64, 1}`.  Three characteristics are set in the definition of `Array{Int64, 1}`:
1. It is an Array
2. It holds values of the type `Int64`
3. It has one dimension

However, we did not know how many elements this array stores yet. To do so, you need to define the array itself: By calling the array constructor with `(undef, 3)` you create an array with the `UndefInitializer`, the default initializer, with `3` elements. After the full construction, you can assign the array to a variable like `arr`. Julia informs you that `arr` is now an array with three elements of type `Int64` with the following values:
``` julia
3-element Array{Int64,1}:
 140372968079008
 140372807975280
 140372834728928
```

### Dimensionality of arrays
Julia supports an unlimited number of dimensions. One and two-dimensional arrays can handled more efficiently by some functions.

#### One dimension (Vector)
You have learned how you can create one dimensional arrays before. These are very common arrays and this is also the reason why the Julia developer has created an extra alias type for them. If you use a one dimensional array you will see that they are declared as a `Vector{Int64}` very often. This `Vector{Int64}` does not mean anything else than `Array{Int64, 1}`. Everything that works for the one dimension array works for the vector and the other way around.

#### Two dimensions (Matrix)
Julia was developed for scientific use. This is obvious in the well-defined array implementation. The language has an extra type alias for matrices. While you can use `Vector{Int64}(undef, 3)` to create a one dimensional array with three values, you can write `Matrix{Int64}(undef, 2, 3)` to create a 2×3 matrix.

Julia supports syntactic sugar for matrix literals. You can use the following literal to create a 2×2 matrix:
``` julia
julia> [1 2
        3 4]
2×2 Array{Int64,2}:
 1  2
 3  4
```

#### Multiple dimensions
Multi dimension arrays are nested arrays. You can think of a three-dimension array `Array{Int64, 3}` to be represented internally as: `Array{Array{Array{Int64, 1}, 1}, 1}`. It is important to know that this is only for visualization. The real implentation is a bit different and much faster.

### Array assignment
To get or set values of an array in Julia, you can use the same syntax. Depending on which side of the assignment the `arr[index]` operator is written, you will read or write the values. With `arr[2] = arr[1]` you will set the second element to the value of the first element. The first element in Julia is always selected with the index `1`.

In our example above we read or write only one element per time. Further, you can get or set a range of values at once. For this you can use the range syntax:
``` julia
julia> arr = [1, 2, 3, 4, 5]
5-element Array{Int64,1}:
 1
 2
 3
 4
 5

julia> arr[2:4] = [22, 33, 44]

julia> arr
5-element Array{Int64,1}:
  1
 22
 33
 44
 5
```

If you have a multi-dimensional array, you can access the elements one by one while you use an index for every dimension separated with a comma. As an alternative, you can get the full second row of `arr` by selecting the second element in the first dimension and get all elements of the second dimension with the range `:` operator.
``` julia
julia> arr2d = ['1' '2' '3'
              'a' 'b' 'c']
2×3 Array{Char,2}:
 '1'  '2'  '3'
 'a'  'b'  'c'

julia> arr2d[2,3]
'c': ASCII/Unicode U+0063 (category Ll: Letter, lowercase)

julia> arr2d[2,:]
3-element Array{Char,1}:
 'a'
 'b'
 'c'
```

### Array functions
The following list shows some important and frequently used functions on arrays:

- `length(arr)`: get the total number of elements within an array. 
- `size(arr)`: get a [tuple](#tuple) that contains the length of the different dimension. `size(arr2d)` will return `(2, 3)` for the 2D array that is defined in [Array assignment](#array-assignment)
- `append!(arr, 1)`: add the element `1` to the existing array `arr`.

## Tuples
Tuples can be used more or less in the same way as an [array](#array), but with some important differences. The first is that neither the length nor the values of the elements within a tuple are changeable after construction. This gives the compiler some possibilities to optimize the code. The second is that tuples can contain multiple types. Any element of a tuple can have its own type. These are the reasons why tuples are used very often in Julia.

### Tuple construction
In Julia two different ways to construct a tuple exist.

1. Use a literal. You can construct a tuple with a literal as `tup = (1, 'a', false)`. The type of this tuple is now `Tuple{Int64,Char,Bool}`.
1. By multiple returns of a function. We have basically already convered this topic in the [Functions](#functions) chapter. If you have a function `f(a, b) = b, a` that returns the input parameters in a different order and call this function with `ret = f(1, 2)`, `ret` is now a tuple that contains the following values `(2, 1)`.

### Get values
Because a tuple can only be constructed once, you can only read values from a tuple. xx meaning of previous sentence was unclear to me, pls check if changes are in accordance with intended meaning xx The most common way is to use the index syntax as we learned in the [array](#array-assignment) section. You can get one value at a time or create a sub-tuple with range access.
 ``` julia
julia> tup = (1, 'a', false)
(1, 'a', false)

julia> tup[3]
false

julia> tup[1:2]
(1, 'a')
```

The second version is typically used if you get multiple return values from a function, but it works with all kinds of tuples.
 ``` julia
julia> tup = (1, 'a', false)
(1, 'a', false)

julia> t1, t2, t3 = tup
(1, 'a', false)

julia> t1
1

julia> t2
'a': ASCII/Unicode U+0061 (category Ll: Letter, lowercase)

julia> t3
false
```

### Tuple functions
Only the `length(tup)` function is implemented for tuples.

## Dictionary
A dictionary, often called dict, is in contrast to an array or tuple a non-linear container type. Any value has a key that directly references it.

### Dictionary construction
Dictionaries have no literal construction. But there exist several constructors that you could call:
``` julia
julia> dict1 = Dict("A"=>1, "B"=>2)
Dict{String,Int64} with 2 entries:
  "B" => 2
  "A" => 1

julia> dict2 = Dict([("A", 1), ("B", 2)])
Dict{String,Int64} with 2 entries:
  "B" => 2
  "A" => 1
```

It is important to know that only immutable types can be used as a key type. Most Julia types are immutable types, except arrays and other dictionaries. If you want to know more about types, please read the [Type](#type) chapter.

### Dictionary assignment
Dictionaries support, parallel to other container types in Julia, the read and write of values by the index operator `[]`. You can use any key as an index. Julia will throw an exception if the index does not exist.
``` julia
julia> dict = Dict("A" => 1, "B" => 2, "C" => 3)
Dict{String,Int64} with 3 entries:
  "B" => 2
  "A" => 1
  "C" => 3

julia> dict["A"]
1

julia> dict["D"]
ERROR: KeyError: key "D" not found
Stacktrace:
 [1] getindex(::Dict{String,Int64}, ::String) at ./dict.jl:477
 [2] top-level scope at REPL[15]:1
```

If you want to write a new key-value pair, to the dictionary you can use the same index syntax. This works in both cases. To update a value or to insert a new pair.
``` julia
julia> dict["B"] = 5
5

julia> dict
Dict{String,Int64} with 3 entries:
  "B" => 5
  "A" => 1
  "C" => 3

julia> dict["D"] = 4
4

julia> dict
Dict{String,Int64} with 4 entries:
  "B" => 5
  "A" => 1
  "C" => 3
  "D" => 4
```

### Dictionary functions
Some frequently used functions for dictionaries are shown here:
- `length(dict)` returns the number of stored keys
- `get(dict, key, default)` will return the value that is stored with the key. If the key does not exist, it will return the default value.
- `delete!(dict, key)` would delete the key and the corresponding value from the dictionary.

# Types
We discussed in the previous [function](#function) chapter how multiple dispatch, the way how Julia selects the correct function, works. The runtime system checks all parameters of a function and selects the best one for us. If you declare a function parameter with a type, all subtypes work as well. The most generic type is `any`. If you do not declare a special type, it is always `any`.
![Julia type hierarchy](julia-type-tree.png)

## Types in function parameters
If you use the `add(a, b)` function, it does not make sense to pass a string to this function. To prevent this you can add a type annotation to the function parameters. Now you can say, I wrote this function, I would never use this function incorrectly. But if a new colleague would use your software as a blueprint, he can use this function in the wrong way. What would happen if you added type annotations to your parameters, and someone else uses the function incorrectly, you can see in the next example:
``` julia
julia> function add(a::Number, b::Number)
         a + b
       end
add (generic function with 1 method)

julia> add(1, 4)
5

julia> add("Hello", "World")
ERROR: MethodError: no method matching add(::String, ::String)
Stacktrace:
 [1] top-level scope at REPL[25]:1
```

The Julia runtime system can not find any correct function to add two strings together. Your program has generated a clear error and not an undefined result. It could be nice to use the `add(a, b)` function to concatenate two strings as well. In Julia this is typically done with the `*` operator or by using the string constructor. Now we want to overload our `add(a, b)` function. The `AbstractString` is the equivalent type to `Number`.
``` julia
julia> function add(a::AbstractString, b::AbstractString)
         string(a, " ", b)
       end
add (generic function with 2 methods)

julia> add("Hello", "World")
"Hello World"
```

During runtime, every variable needs to be assigned an exact type. These are the leaves of the type tree above. If we use our `add(a, b)` function, you can use the `typeof(var)` function to get the exact runtime type of a parameter. Now you can see that the abstract types like `Number` are used only at the function selection time. The executed method would be compiled and optimized with the explicit types.
``` julia
julia> function add(a::Number, b::Number)
         println("Type of a: ", typeof(a))
         println("Type of b: ", typeof(b))
         a + b
       end
add (generic function with 2 methods)

julia> add(1, 3)
Type of a: Int64
Type of b: Int64
4

julia> add(2.0, 50)
Type of a: Float64
Type of b: Int64
52.0
```

## Types in Arrays
While type annotations of function parameters are used to select the correct function, explicit type declaration in arrays is important to allocate the correct amount of memory. You can create an array with all available types. This also includes the abstract types.
``` julia
julia> arr1 = Array{Int64, 1}(undef, 0)
0-element Array{Int64,1}

julia> arr2 = Array{Number, 1}(undef, 0)
0-element Array{Number,1}

julia> append!(arr1, 15)
1-element Array{Int64,1}:
 15

julia> append!(arr2, 15)
1-element Array{Number,1}:
 15
```

Both arrays have the same length and contain the same values. But the first one `Array{Int64, 1}` is much faster. The reason can found under the hood of Julia. It is highly recommended to use the most specific type possible in combination with arrays.

## Structured types
Julia comes with a lot of different types. But sometimes this not enough. For this reason, you can create structured types by your self. The declaration of a type starts with `struct` followed by the new type name and the components.
``` julia
julia> struct Grid
         lons
         lats
       end

julia> g = Grid(25, 12)
Grid(25, 12)

julia> g2 = Grid("Lons", "Lats")
Grid("Lons", "Lats")
```

In the last example is shown that sometimes it is not useful to allow all kind of types for a component. To write more secure code you can add type annotations to the component names. If you sign the wrong type, Julia will throw an error.
``` julia
julia> struct Grid2
         lons::Int64
         lats::Int64
       end

julia> g3 = Grid2(25, 12)
Grid2(25, 12)

julia> g4 = Grid2("Lons", "Lats")
ERROR: MethodError: Cannot `convert` an object of type String to an object of type Int64
Closest candidates are:
  convert(::Type{T}, ::T) where T<:Number at number.jl:6
  convert(::Type{T}, ::Number) where T<:Number at number.jl:7
  convert(::Type{T}, ::Ptr) where T<:Integer at pointer.jl:23
  ...
Stacktrace:
 [1] Grid2(::String, ::String) at ./REPL[8]:2
 [2] top-level scope at REPL[9]:1
```

To get the stored values, Julia provides the `.` operator.
``` julia
julia> g.lons
25

julia> g.lats
12
```

All the structures created above could not change the values after construction. The new types are immutable. To create a mutable structure you have to add the `mutable` keyword in front of the declaration. Now you can change the values of the components doing runtime.
``` julia
julia> mutable struct Grid3
            lons::Int64
            lats::Int64
       end

julia> g5 = Grid3(25, 12)
Grid3(25, 12)

julia> g5.lons = 18
18

julia> g5
Grid3(18, 12)
```

A `mutable struct` can not use as a key in a dictionary. For more details see [Dictionary](#dictionary)

## Additional Information
For more information I recommend reading the [official Julia documentation](https://docs.julialang.org/en/v1/manual/types/).

# Speed
The Julia developer propagates that Julia is a fast language. But all languages like these, not only the programming language itself must be fast. The program that you write must give the compiler the possibility to make fast running code out of it. It is easy to write slow Julia code but with some basic knowledge, you can write fast programs without thinking to much about it.

In the next sections, we want to discuss the most important topics in descending order.

## Type stable functions
The most important Julia feature that gives the compiler the possibility to optimize your code are type stable functions. The following `sum(arr)` function illustrate the problem.
``` julia
julia> function sum(arr::Vector{T}) where {T <: Number}
       sum = 0

       for i in 1:length(arr)
            sum = sum + arr[i]
       end

       return sum
       end
```
We do not want to discuss the `Vector{T}` and `where {T <: Number}` syntax in more detail here. It gives us the safety that only arrays that contain numbers can pass to this function.

Let's create some example data sets:
``` julia
julia> arrFloatEmpty = Vector{Float64}(undef, 0)
0-element Array{Float64,1}

julia> arrIntEmpty = Vector{Int64}(undef, 0)
0-element Array{Int64,1}

julia> arrFloat = [1.0, 2.0, 2.2, 3.0]
4-element Array{Float64,1}:
 1.0
 2.0
 2.2
 3.0

julia> arrInt = [1, 2, 2, 3]
4-element Array{Int64,1}:
 1
 2
 2
 3
```

And pass this sets to our sum function:
``` julia
julia> sum(arrFloatEmpty)
0

julia> sum(arrIntEmpty)
0

julia> sum(arrFloat)
8.2

julia> sum(arrInt)
8
```

The results look good. But if we try to take a look at these functions from Julia's point of view we can see a problem. Julia creates from this one `sum(arr)` function, two different methods. One that can handle arrays that contain integer values and one that could handle arrays of floats. Let's analyze the result types if we pass the float arrays to our `sum` function.
``` julia
julia> typeof(sum(arrFloat))
Float64

julia> typeof(sum(arrFloatEmpty))
Int64
```

We can see that in both cases where we pass a float array to our function we get different resulting types depending of the array length. To see what happens inside our function we can create a debug version that prints some useful logs. For every step, we print the value of `sum` and afterward the type of the variable.
``` julia
julia> function  sumDebug(arr::Vector{T}) where {T <: Number}
       sum = 0
       println(sum)
       println(typeof(sum))

       for i in 1:length(arr)
            sum = sum + arr[i]
            println(sum)
            println(typeof(sum))
       end

       return sum
       end
```

Now we can see what happens inside the function:
``` julia
julia> sumDebug(arrFloat)
0
Int64
1.0
Float64
3.0
Float64
5.2
```

You can see that the type of the variable sum changes during the execution. This makes it the compiler impossible to optimize the machine code. To solve this problem we need to assign `sum` with `0.0` opposite of `0`. Now we can write different functions for arrays of floats and arrays of integers how we discussed in the [Typed Parameters](#typed-parameters) section. Or we use the Julia way. The language defines an extra `zero(x)` function. `zero(x)` will always return the correct zero values for the passed type. We can adapt our function to see what the more advanced function does.
``` julia
function  sumAdvancedDebug(arr::Vector{T}) where {T <: Number}
         sum = zero(T)
         println(sum)
         println(typeof(sum))

         for i in 1:length(arr)
            sum = sum + arr[i]
            println(sum)
            println(typeof(sum))
         end

         return sum
       end
```

If we call this function, you can see that the variable `sum` does not change his type.
``` julia
julia> sumAdvancedDebug(arrFloat)
0.0
Float64
1.0
Float64
3.0
Float64
5.2
Float64
8.2
Float64
8.2
```

## Vectorization aka broadcast
Vectorization is the best way in scientific computation to speed up your project. This is the reason why Julia tries by its on to vectorize (broadcast) your code in as many situations as possible. But you as the developer can give the compiler a hint in the right direction.

To analyze the problem we use the following example code. We want to multiply two one dimension arrays and store the result in a third with the same size.
``` julia
julia> out = Vector{Float64}(undef, 100)
julia> in1 = ones(Float64, 100)
julia> in2 = ones(Float64, 100)

julia> for i in 1:length(out)
         out[i] = in1[i] + in2[i]
       end

julia> out[25]
2.0
```

For loops are quite fast in Julia. But the compiler can sometimes increase the speed again. For this, you can use the very simple broadcast syntax. By adding a dot (`.`) in front of an operator you tell the compiler that this operation should be evaluated for every single element of the involved arrays.
``` julia
 out .= in1 .+ in2
```

This works also if you want to increase our example by multiplying a scalar value first to the second array before we add these to `in1`.
``` julia
julia> s = 22.215
22.215

julia> out .= in1 .+ in2 .* s
```

To use vectorization, the output array `out` must define like this `Vector{Float64}(undef, 100)` before using it. 

### The broadcast macro

If you have larger expressions, the dot syntax can be hard to read. To solve this problem Julia supports the `@.` macro. This macro can be written in front of the code line and the whole expression will be wrapped into the broadcast syntax.
``` julia
julia> @. out = in1 + in2 * s
```

To see what the Julia compiler internal dose with the `@.` macro we can use another macro:
``` julia
julia> @macroexpand @. out = in1 + in2 * s
:(out .= (+).(in1, (*).(in2, s)))
```

The syntax of the operators is a bit different, these operators would be called like functions, but it is good to see that all the operators get one dot and will use the broadcast feature.

Be careful by using the broadcast macro in combination with functions. It could be safe to cache the function result and use it later in a broadcast macro. The context that is created by the `let` keyword make sure that the memory that is used by the `tmp` variable is released by leaving the block aafter the `end`.
``` julia
julia> f(arr) = arr .* 2.2
f (generic function with 2 methods)

julia> let
         tmp = f(in2)
         @. out = in1 + tmp * 22.215
       end
```

### Additional information
Broadcasting is a big topic in Julia. If you want more information, you can read the [official documentation](https://docs.julialang.org/en/v1/base/arrays/#Broadcast-and-vectorization).

# Libraries
In this chapter, I want to give you a short outlook on different Julia libraries. These libraries help you to make your project as good as possible. Some modules are specially designed for climate science while other modules extend the Julia feature list. All the sections are not compatible with the original documentation. This is more a beginning for deeper research.  

## cdi.jl
The *climate data interface* is developed by the Max-Planck-Institut für Meteorologie since 2006. The original in C written library gives the user a fast, read and write, interface for NetCDF and GRIB 2 files. The *climate data interface* is used in different internal applications like the *climate data operators* (*CDO*). While the original library has a long history, the new developed *cdi.jl* Julia library is relatively new. 

The cdi.jl library gives the user a typical Julia interface for reading and writes climate data based on the C *climate data interface*. This means that the Julia interface is not slower than a C application.

### Installation
The installation process is not as easy as normal Julia packages. This gives the user a big amount of flexibility but might be changed in the future.

#### Ubuntu / Debian
#### MPI-M
#### Mistral
The following guide show you how to install the original libcdi followed by cdi.jl on mistral. To follow this guide you have to run the commands on a login node.

1. Load necessary modules
```bash
. /sw/rhel6-x64/etc/profile.mistral
module use /sw/spack-rhel6/spack/modules/linux-rhel6-haswell
module load libtool autoconf automake gcc/6.4.0
```

2. Clone the libcdi repository to your local account
```bash
git clone https://gitlab.dkrz.de/mpim-sw/libcdi.git
cd libcdi
git checkout develop
./autogen.sh
```

3. Create a folder where the final library would save. A typical place should be `~/local`. This is used in the following guide. If you want to use a different location, please change the path.
```bash
mkdir ~/local
autoreconf -i
./configure --prefix="~/local" --with-netcdf=/sw/rhel6-x64/netcdf/netcdf_c-4.6.1-gcc64 --with-eccodes=/sw/rhel6-x64/eccodes/eccodes-2.6.0-gcc64
make
make install
```

4. Export the newly created library to the `LD_LIBRARY_PATH` system variable. If you want to use cdi.jl in the future again, it could be a good idea to write the following lines into your `~/.bashrc` file. If you did these, do not forget to reload the config with `source ~/.bashrc`.
```bash
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:"~/local/lib"
```

5. To install cdi.jl open Julia and add this link as a new module:
``` julia
(@v1.4) pkg> add https://gitlab.dkrz.de/m300859/cdi.jl.git
```

### Basic usage
The following example cams from the official cdi.jl examples [here](https://gitlab.dkrz.de/m300859/cdi.jl/-/blob/master/examples/cdi_read.jl).
``` julia
using cdi.cdic
using cdi.cdiDatasets
using Dates

# help function to test a variable name
hasVar(ds, name) = haskey(ds, name) || @warn "$name is not define"

const path = joinpath(@__DIR__, "example.nc")

# laod *.nc file to read the data
ds = cdiDataset(path)

# Print the created cdiDataset with all metadata 
print(ds)

# test if the variables exists
varID_1 = 1
#hasVar(ds, varID_1) # haskey is only available for strings
hasVar(ds, "varname2")

# create a buffer to load 'varname1'
var2 = Array{Float64}(undef, size(ds["varname2"]))

# loop over the time steps 
while getNextTimestep(ds)
    # get the verification date and time 
    @info "Step " * string(getTimestep(ds))
    @info DateTime(ds)

    # read 'varname1' and allocate storage
    # have acess via the varID
    var1 = loadData(ds[varID_1])
    # read 'varname2' into the pre allocated buffer
    loadData!(var2, ds["varname2"])
end
```

### Additional information
There is currently no real cdi.jl documentation. But all supported features are shown in the well-documented examples at the public [GitLab repository](https://gitlab.dkrz.de/m300859/cdi.jl). 

## NCDatasets.jl
The NCDatasets.jl library is a NetCDF file interface. The Julia implementation is based on the ncdf4 library and is written by Alexander Barth.

### Installation
You can easily install the module by the Julia package manager shown in the chapter [package manager](#ackage-manager) before.
``` julia
(@v1.4) pkg> add NCDatasets
```

### Basic usage
The following example comes from the original GitHub repository and shows how to load a data set and extract some values.
``` julia
using NCDatasets

# The mode "r" stands for read-only. The mode "r" is the default mode and the parameter can be omitted.
ds = Dataset("/tmp/test.nc","r")
v = ds["temperature"]

# load a subset
subdata = v[10:30,30:5:end]

# load all data
data = v[:,:]

# load all data ignoring attributes like scale_factor, add_offset, _FillValue and time units
data2 = v.var[:,:]


# load an attribute
unit = v.attrib["units"]
close(ds)
```

### Additional information
You can find the official GitHub repository with a short documentation [here](https://github.com/Alexander-Barth/NCDatasets.jl).
